# Présentation de GitLab

GitLab est un gestionnaire de projets de développements collaboratifs.
Il intègre notamment une panoplie d'outils visant à faciliter les
différents aspects liés au développement d'une application que sont la
gestion des versions du code, la collaboration entre plusieurs
contributeurs, la documentation et le partage du projet.  Le FIL,
département d'Informatique de l'Université de Lille, met à votre
disposition un dépôt GitLab que vous pouvez utiliser librement pour
réaliser vos TPs et projets en collaborant efficacement avec vos
camarades.  

Ce service est disponible à l'adresse suivante :

https://gitlab-fil.univ-lille.fr/

vous pouvez vous y connecter avec vos codes d'accès.

Ce document est une rapide présentation de l'utilisation de GitLab.
Durant la formation, nous utiliserons GitLab pour gérer les travaux
pratiques et les rendus dans les 5 blocs de formation.

De plus, Git est un outil très couramment utilisé dans des projets
de développement collaboratifs.

Pour des explications plus poussées sur le fonctionnement de GitLab 
ou des usages plus avancés, vous pouvez consulter la documentation
en ligne de https://about.gitlab.com).

> Le présent document identifie les menus de l'interface en ligne par
> les termes anglais.  
> Vous pouvez changer la langue de l'interface via le menu
> `Preference` (_Préférence`) de votre `Profile` (`Profil`), section
> `Localization` 

# Étape 1 : créer un dépôt distant

Connectez vous sur l'interface en ligne de GitLab puis cliquez 
sur `New project` et choisissez `create blank project`. Indiquez un `Project Name` pour votre projet.

Indiquez ensuite une description du projet.

Cochez qu'il s'agit d'un projet `Private`, puis validez en cliquant 
sur `Create Project`.

Vous pouvez ensuite donner les droits à votre binôme sur ce dépôt
pour qu'il puisse accéder et contribuer au code collaborativement.

Il faut que votre binôme se soit connecté au moins une fois à GitLab.
Ensuite, cliquez sur le menu `Members` (en bas à gauche, au niveau
de la roue crantée), renseignez l'identifiant de votre binôme 
(son *login*), changez les droits d'accès pour `Maintainer`, puis 
cliquez sur le bouton `Add to project`.

Renouvelez l'opération pour enregistrer votre enseignant de TD/TP
en tant que `Developer` afin qu'il puisse accéder à votre travail. 

# Étape 2 : créer un dépôt local

Votre projet est désormais créé sur le dépôt distant et vous allez
pouvoir en créer la version locale dans votre espace de travail.

## Identification par ssh

Avant cela, afin de faciliter l'authentification et vous éviter
de saisir votre mot de passe lors de chaque opération, vous 
pouvez enregistrer une clé publique SSH qui sera utilisée par 
GitLab pour vous authentifier.

Cette étape est optionnelle mais fortement recommandée car elle
facilite grandement l'utilisation quotidienne de GitLab.

### Généralités concernant RSA

RSA est un système de chiffrement largement utilisé. Une compréhension
des grands principes de RSA permet de résoudre certaines situations.

Les clés de chiffrement RSA vont par paire : Il y a une clé privée
, que vous êtes le seul à posséder, et une clé publique que l'on 
fournit à toute personne ou système avec lequel on est susceptible
d'échanger des messages.

Le principe d'un chiffrement asymétrique est le suivant : 

-   Un message chiffré avec la clé publique est très facilement déchiffrable
    avec la clé privée (et quasiment impossible sans).
-   Un message chiffré avec la clé privée est très facilement déchiffrable
    avec la clé publique (et quasiment impossible sans).

Ici, on va utiliser RSA pour nous authentifier :

-   le serveur sur lequel on désire s'authentifier dispose de la clé 
    publique,
-   Comme nous disposons de la clé privée, nous sommes les seuls à 
    pouvoir déchiffrer ce qu'il nous envoie,
-   Cela permet donc une authentification sans mot de passe,
-   Le protocole est transparent pour l'utilisateur.

### Enregistrement

Pour enregistrer une clé publique, choisissez `Profile Settings`
dans le menu en haut à droite, puis sur l'onglet `SSH keys`. Dans 
le champ `Key`, copiez votre clé publique SSH en faisant un 
copier-coller du contenu retourné par la commande :

    $ cat ~/.ssh/id_rsa.pub
    ssh-rsa AAAAB3NzaC1yc2EAAAABIwABAQEAklOUpkDHrfHY17SbrmTIpNLTGK9Tjom/BWDSU
    GPl+nafzlHDTYW7hdI4yZ5ew18JH4JW9jbhUFrviQzM7xlELEVf4h9lFX5QVkbPppSwg0cda3
    Pbv7kOdJ/MTyBlWXFCR+HAo3FXRitBqxiX1nKhXpHAZsMciLq8V6RjsNAQwdsdMFvSlVK/7XA
    t3FaoJoAsncM1Q9x5+3V0Ww68/eIFmb1zuUFljQJKprrX88XypNDvjYNby6vw/Pb0rwert/En
    mZ+AW4OZPnTPI89ZPmVMLuayrD2cE86Z/il8b+gw3r3+1nKatmIkjn2so1d01QraTlMqVSsbx
    NrRFi9wrf+M7Q== timoleon@laptop.local

### Génération d'une paire de clés.

Si un message d'erreur vous indique que le fichier n'existe pas, pas de panique : vous
devez créer une paire de clés publique/privée en utilisant la commande :

    $ ssh-keygen
    Generating public/private rsa key pair.
    Enter file in which to save the key (/Users/timoleon/.ssh/id_rsa):
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /Users/toto/.ssh/id_rsa.
    Your public key has been saved in /Users/toto/.ssh/id_rsa.pub.
    The key fingerprint is:
    43:c5:5b:5f:b1:f1:50:43:ad:20:a6:92:6a:1f:9a:3a timoleon@laptop.local

Une fois votre paire de clés SSH créée, vous pouvez copier le contenu
de la clé publique (le fichier `.pub`) dans GitLab pour qu'il vous reconnaisse
ensuite. Attention : il est possible que GitLab prenne quelques secondes
ou minutes pour reconnaître votre clé publique SSH.

Vous pouvez créer et enregistrer dans GitLab plusieurs clés publiques
SSH pour vous identifier sur les différentes machines que vous utilisez 
à l'université ou chez vous avec votre machine personnelle.

## Première synchronisation du dépôt

Vous pouvez maintenant synchroniser votre dépôt local avec le dépôt
distant créé sur GitLab.  Pour ce faire,  dans votre
espace de travail positionnez-vous dans le dossier dans lequel vous
voulez placer vos travaux.

Identifiez ensuite l'adresse de votre dépôt à partir du bouton **Clone** sur la droite de
la fenêtre :

![img](./git-adresse.png "récupérer l'adresse")

Le petit bouton à droite de cette adresse permet de la coller dans le presse-papier.

La première synchronisation se réalise grâce à la commande `git clone`. Il faut lui indiquer 
l'adresse du dépôt, si vous avez nommé celui-ci `NOM_DE_VOTRE_DEPOT`, cette adresse ressemblera
 à ce qui est indiqué :

    git clone git@gitlab-fil.univ-lille.fr:[VOTRE_LOGIN]/NOM_DE_VOTRE_DEPOT.git NOM_DU_DOSSIER_LOCAL
    Clonage dans 'NOM_DU_DOSSIER_LOCAL'...
    warning: Vous semblez avoir cloné un dépôt vide.
    Vérification de la connectivité... fait.

Cette commande permet de créer un dépôt Git local synchronisé avec votre dépôt distant
et le place dans un dossier nommé `NOM_DU_DOSSIER_LOCAL` (le dernier argument de la ligne de commande)

Cette opération n'est à faire qu'une seule fois lors de la création du dépôt local (mais vous pouvez 
créer autant de dépôt locaux que vous souhaitez). Nous allons maintenant voir comment travailler avec
ce dépôt local et maintenir sa synchronisation avec le dépôt distant.

**Attention** si vous n'avez pas saisi de clé SSH pour vous identifier sur GitLab, vous devez cloner
votre projet en utilisant l'authentification par `https` : 

    git clone https://gitlab-fil.univ-lille.fr:[VOTRE_LOGIN]/NOM_DE_VOTRE_DEPOT.git NOM_DU_DOSSIER_LOCAL

Il vous faudra alors fournir votre login et mot de passe lors des opérations vers le dépôt distant.
Cela n'est donc pas conseillé car peu pratique à l'usage.

# Ajouter des fichiers

Un dépôt Git est par exemple destiné à héberger du code source et non
pas des fichiers compilés ou des fichiers de sauvegarde de votre
éditeur de textes.  
Pour éviter de stocker de tels fichiers par erreur, nous allons enregistrer un fichier `.gitignore` qui va 
indiquer à Git les fichiers qu'il doit ignorer lors de opérations d'ajout. La façon la plus simple
de générer ce fichier est de vous rendre sur le site <https://www.gitignore.io>, d'indiquer que vous
réalisez un projet Python (ou autre), de préciser le(s) système(s) d'exploitation utilisé(s),
 le(s) éditeur(s), les outils, etc. puis de cliquer sur le bouton `Create` :

![img](./gitignore.png)

Il suffit ensuite de copier le contenu qui vous est retourné dans un fichier `.gitignore` stocké 
**à la racine du dossier** correspondant à votre dépôt local. À nouveau cela est fait une fois pour
toutes.

La commande `git status` permet de connaitre l'état du dépôt local par rapport au distant. Essayer 
la sur votre dépôt (vous devez vous trouver dans le dossier du dépôt local, ou l'un de ses 
sous-dossiers (quand il y en a).

Git détecte les fichier (ici uniquement `.gitignore`) qui se trouvent dans votre dépôt local et 
qui ne sont pas encore présents sur le dépôt distant :

    $ git status
    Sur la branche master
    Validation initiale
    Fichiers non suivis:
      (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)
    
            .gitignore
    
    aucune modification ajoutée à la validation mais des fichiers non suivis sont présents 
    (utilisez "git add" pour les suivre)

**Trois étapes** sont nécessaires pour ajouter ces fichiers dans le dépôt distant. Ce sont les 
mêmes à appliquer pour mettre à jour un fichier modifié localement.

## Ajouter les fichiers

La première étape consiste à ajouter les fichiers à synchroniser – les
nouveaux fichiers et les fichiers modifiés – en utilisant
la commande `git add`, comme suggéré dans la trace obtenue ci-dessus :

    git add .gitignore

plusieurs commandes `add` peuvent être enchaînées si plusieurs fichiers sont à synchroniser. On 
peut aussi ajouter un dossier ce qui aura pour conséquence d'ajouter tout son contenu.

Exécuter la commande `git status` et constatez le changement de message par rapport au précédent.

Créez un fichier `Readme.md` dans votre dépôt, placez-y un contenu tel que 

    dépôt des TP du bloc 3 du binôme Xxx XXX et Yyy YYY

Utilisez la commande `git status` pour vérifier que ce fichier est nouveau et non suivi, puis 
ajouter le comme fichier à synchroniser.

## Valider les changements

La seconde étape consiste à valider (et à leur donner un numéro de version) l'ensemble des 
changements qui ont été accumulés par les différents `add`. Cela se fait grâce à la commande 
`git commit`. Chaque opération de *commit* doit être accompagnée d'un message :

    $ git commit -m "initialisation du dépôt : .gitignore et Readme.md"
    [master (commit racine) 00fa862] initialisation du dépôt : .gitignore et Readme.md
     2 files changed, 0 insertions(+), 0 deletions(-)
     create mode 100644 .gitignore
     create mode 100644 Readme.md

Le message de *commit* passé en paramètre (option `-m`) n'est pas à négliger car il doit 
synthétiser la nature de la modification qui a été apportée sur le code du projet. Ces messages
apparaîtront dans le journal du dépôt. Ils sont utilisés par les autres collaborateurs au projet
 pour prendre connaissance de ce qui a été fait.  Dans le cas où l'on n'indique pas l'option `-m` un
éditeur s'ouvre pour proposer de rédiger le message de *commit*. Certains considèrent que
 l'option `-m` n'est pas une bonne pratique car elle incite à faire des messages trop courts.

À cet instant, si vous vous connectez sur l'interface web de GitLab vous constaterez que le
 dépôt est toujours vide : consultez l'onglet `Repository` de votre dépôt.

En effet, pour l'instant les fichiers sont simplement versionnés par Git dans le dépôt local mais
ils n'ont pas toujours été transmis au dépôt distant. C'est le rôle de la troisième étape.

## Envoyer les changements

La troisième étape correspond donc à l'envoi effectifs des changements
depuis le dépôt local vers le dépot distant. On utilise pour cela la
commande `push` :

    $ git push -u origin master
    Décompte des objets: 4, fait.
    Delta compression using up to 4 threads.
    Compression des objets: 100% (3/3), fait.
    Ecriture des objets: 100% (4/4), 572 bytes | 0 bytes/s, fait.
    Total 4 (delta 0), reused 0 (delta 0)
    To git@gitlab-etu.fil.univ-lille1.fr:[VOTRE_LOGIN]/NOM_DE_VOTRE_DEPOT.git
       00fa862..fbc682f  master -> master
    La branche master est paramétrée pour suivre la branche distante master depuis origin.

Le message indique que tout s'est correctement déroulé et que les
changements regroupés dans le *commit* ont été transmis au dépot
distant.

L'exécution de la commande `git status` permet de vérifier que
le dépôt local et le dépôt distant sont désormais synchronisés.

En consultant le `Repository` dans l'interface web de votre dépôt
GitLab vous pouvez en effet constater que les fichiers ont été
ajoutés. 

>>>
Vous pouvez aussi remarquer que le contenu fichier
`Readme.md` est affiché dans cette page d'accueil de
votre projet. Ce fichier d'extension `.md` est format Markdown. Voyez
les documentations suivantes : 

* [a Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [GFM - GitLab Flavored Markdown (GFM)](https://docs.gitlab.com/ee/user/markdown.html)
>>>

La dernière phrase du message du *push* est une conséquence de
l'utilisation de l'option `-u`. Elle signifie que les prochaines commandes `git push` pourront se
 faire sans paramètre : par défaut on effectuera ces opérations sur la branche *master* vers le dépôt
 distant désigné par *origin*. Les branches sont des notions très importantes dans le bon usage de
 Git mais dans cette première approche nous travaillerons sur la seule branche *master*.

## Synthèse

Ces trois étapes `git add`, `git commit` et `git push` sont nécessaires pour transmettre les changements
que vous réaliserez localement vers le dépôt distant.

Revoyons ces étapes à travers un petit exercice :

1.  Créez dans votre dépôts local un dossier nommé `TP0` et placez-y un nouveau fichier `a.txt` 
     (peu importe son contenu). Modifiez également votre fichier `Readme.md` (ajoutez un `#` en
    début de la première de texte par exemple) ;
2.  Consultez le résultat de la méthode `git status`;
3.  Faites le nécessaire pour synchroniser les modifications du   dépôt local vers le dépôt distant ;
4.  Vérifiez avec `git status` que tout est à jour.

# Récupérer des fichiers

Nous avons vu comment envoyer des modifications vers le dépôt distant,
reste à savoir comment réaliser l'opération inverse : synchroniser un
dépôt local avec les modfications du dépôt distant.

Commençons par créer un second dépôt local. Deux solutions s'offrent à
vous : soit votre binôme se connecte et crée son propre dépôt local en
clonant le projet comme décrit précédemment, soit vous créez un
second dépôt local dans votre espace de fichiers.

Une fois l'une ou l'autre des ces actions effectuée vous disposez de
deux dépôts locaux synchronisés avec le dépôt distant.

Dans l'un de ces deux dépôts locaux faites un changement (modification
d'un fichier existant ou création d'un nouveau fichier) puis envoyez
la vers le dépôt distant.

Le second dépôt se retrouve alors nécessairement désynchronisé avec le
dépôt distant. Il faut donc dans ce dépôt récupérer la nouvelle
version du dépôt distant afin de pouvoir continuer à y travailler avec
une version à jour. Pour cela il faut utiliser la commande `git  pull` 
(dans le second dépôt !). Si vous aviez par exemple modifié le
fichier `Readme.md` dans le premier dépôt vous obtenez alors
dans le second dépôt une trace de la forme :

    $ git pull
    remote: Counting objects: 3, done.\
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 3 (delta 0), reused 0 (delta 0)
    Dépaquetage des objets: 100\% (3/3), fait.
    Depuis gitlab-etu.fil.univ-lille1.fr:[LOGIN]/NOM_DE_VOTRE_DEPOT
        f5d366c..f4d6258  master     -> origin/master
    Mise à jour f5d366c..f4d6258
    Fast-forward
     Readme.md | 2 +-
     1 file changed, 1 insertion(+), 1 deletion(-)

Dans ce dépôt vous pouvez alors constater que vous avez bien la
dernière version des fichiers modifiés et/ou ajoutés sur
l'autre dépôt.

La commande `git log` vous permet de consulter le journal des *commit* et vous
y voyez apparaitre les différents messages utilisés lors de ces opérations.

## Pour résumer

Quand vous voudrez travailler dans votre dépôt local. Il faudra
commencer par le mettre à jour et récupérer les modifications qui ont
été enregistrées sur le dépôt distant, il vous suffira d'exécuter la
commande :

    $ git pull

C'est un réflexe à acquérir que de systématiquement commencer
par exécuter cette commande avant tout travail dans le dépôt
local.

Vous apportez ensuite localement vos modifications en les éditant localement.

Puis, quand vous souhaitez valider et partager ces
modifications fichier avec votre binôme, il suffit de l'envoyer vers
le dépôt distant en exécutant la séquence de commandes :

    $ git add mon_fichier.ext mon_dossier/
    $ git commit -m "Mon message de commit"
    $ git push

Bien entendu, Git ne se limite pas à ces quelques commandes (voyez
`git help -a`, par exemple `git rm` permet de supprimer un fichier du dépôt, etc).  
N'hésitez pas à consulter la documentation de Git pour découvrir son fonctionnement 
et les différentes commandes utilisables.

# Résoudre les conflits

En général, un conflit se produit lorsque vous modifiez un fichier localement sans
avoir synchronisé le dépôt distant.

Considérons encore les deux dépôts locaux créés précédemment. Pour simplifier, nous les
nommerons `depot_1` et `depot_2`.

Modifiez le fichier `Readme.md` dans `depot_1` (en ajoutant par exemple la ligne `## Titre de second niveau` )
puis envoyez les modifications de `depot_1` dans le dépôt distant :

    ~/depot_1$ git add Readme.md
    ~/depot_1$ git commit -m "modification du readme dans depot_1"
    [master c3e7101] modification du readme dans depot_1
     1 file changed, 3 insertions(+), 1 deletion(-)
    ~/depot_1$ git push
    Décompte des objets: 3, fait.
    Écriture des objets: 100% (3/3), 305 bytes | 0 bytes/s, fait.
    Total 3 (delta 0), reused 0 (delta 0)
    To git@gitlab-ens.fil.univ-lille1.fr:[LOGIN]/NOM_DE_VOTRE_DEPOT
       f75c3b1..c3e7101  master -> master

Puis dans `depot_2` (qui est  désynchronisé) modifiez aussi le fichier `Readme.md` (en ajoutant par
exemple la ligne `ceci est un **texte en gras**`. `depot_2` est alors désynchronisé. Essayons d'envoyer 
nos modifications :

    ~/depot_2$ git add Readme.md
    ~/depot_2$ git commit -m "modification dans depot_2"
    [master 2e20d2a] modification dans depot_2
     1 file changed, 3 insertions(+), 1 deletion(-)
    ~/depot_2$ git push
    To git@gitlab-ens.fil.univ-lille1.fr:[LOGIN]/NOM_DE_VOTRE_DEPOT
     ! [rejected]        master -> master (fetch first)
    error: impossible de pousser des références vers 'git@gitlab-ens.fil.univ-lille1.fr:[LOGIN]/NOM_DE_VOTRE_DEPOT'
    astuce: Les mises à jour ont été rejetées car la branche distante contient du travail que
    astuce: vous n'avez pas en local. Ceci est généralement causé par un autre dépôt poussé
    astuce: vers la même référence. Vous pourriez intégrer d'abord les changements distants
    astuce: (par exemple 'git pull ...') avant de pousser à nouveau.
    astuce: Voir la 'Note à propos des avances rapides' dans 'git push --help' pour plus d'information.

Le message est clair : la modification est rejetée en raison d'une modification sur la copie distante.

`git` suggère alors de faire un `git pull` :

    ~/depot_2$ git pull
    remote: Enumerating objects: 5, done.
    remote: Counting objects: 100% (5/5), done.
    remote: Total 3 (delta 0), reused 0 (delta 0)
    Dépaquetage des objets: 100% (3/3), fait.
    Depuis gitlab-ens.fil.univ-lille1.fr:[LOGIN]/NOM_DE_VOTRE_DEPOT
       f75c3b1..c3e7101  master     -> origin/master
    Fusion automatique de Readme.md
    CONFLIT (contenu) : Conflit de fusion dans Readme.md
    La fusion automatique a échoué ; réglez les conflits et validez le résultat.

cela ne fonctionne pas car nous n'avons pas résolu le conflit. Éditez le fichier `Readme.md` :

    # Mon fichier
    
    <<<<<<< HEAD
    **texte en gras**
    =======
    ## titre de second niveau
    >>>>>>> c3e710112b6aedbff7a03a8bdae53f53fcbb928e

Nous constatons que le(s) conflit(s) est détecté et pouvons choisir la version à conserver pour
envoyer dans le dépôt distant. Par exemple :

    # Mon fichier
    
    **texte en gras**

(supprimez les lignes commençant par des chevrons et choisir une des deux versions).

Il faut ensuite effectuer de nouveau les opérations habituelles pour envoyer les modifications :

    ~/depo_2$ git add Readme.md
    ~/depot_2$ git commit -m "résolution du conflit"
    [master e75698c] résolution du conflit
    ~/depot_2$ git push
    
    Décompte des objets: 4, fait.
    Delta compression using up to 8 threads.
    Compression des objets: 100% (2/2), fait.
    Écriture des objets: 100% (4/4), 423 bytes | 0 bytes/s, fait.
    Total 4 (delta 1), reused 0 (delta 0)
    To git@gitlab-ens.fil.univ-lille1.fr:[LOGIN]/NOM_DE_VOTRE_DEPOT
       c3e7101..e75698c  master -> master

En suite il ne faut pas oublier de synchroniser `depot_1`, sinon nous aurons de nouveau un problème
de synchronisation lors de la prochaine modification de ce dépôt.

# Forker un dépôt

La plupart des TPs sur lesquels vous travaillerez nécessitent de "forker" un dépôt.
Il s'agit d'une opération permettant de copier un dépôt et d'inclure la copie dans
votre espace de nom : vous devenez `Maintainer` du dépôt copié.

Voici le déroulement de la plupart des travaux pratiques :

-   Les enseignants diffusent un sujet de tp sous forme d'un dépôt.
-   Les étudiants forkent ce dépôt et travaillent sur ce dépôt.
-   Les enseignants peuvent récupérer automatiquement l'ensemble des dépôts forkés 
    à partir du dépôt initial.

Pour forker un dépôt, il suffit de vous rendre sur l'interface web du dépôt, 
 puis d'appuyer sur le bouton `Fork` (`Créer une divergence` en
 français).

Vous devez ensuite fournir l'espace de nom dans lequel vous allez forker le projet : sélectionnez 
votre propre espace de nom.

Ensuite vous pouvez cloner le dépôt forké dans un répertoire local.

# Clés ssh sous windows

Pour [gérer les clés ssh sous windows](https://docs.ovh.com/fr/public-cloud/creation-des-cles-ssh/).

<!-- eof -->
