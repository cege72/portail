
"""
Wa-Tor

author : Jean-Christophe Routier jean-christophe.routier@univ-lille.fr
Département Informatique Université de Lille
"""


import random

# some global constants
SHARK = 'S'
TUNA = 'T'
EMPTY = ' '

# simulation global data
SHARK_GESTATION = 5
TUNA_GESTATION = 2
SHARK_ENERGY = 3

# following are initialized when sea is created
SEA_WIDTH = -1
SEA_HEIGHT = -1

# numbers of living tunas and sharks, initially none
NB_TUNAS = 0
NB_SHARKS = 0

def create_shark():
    '''
    create a shark data, it is a dictionnary with 3 fields :
    'char'acter for display, 'gestation' period and, 'energy'
    @return the created shark
    >>> fish = create_shark()
    >>> fish['char'] == SHARK
    True
    >>> fish['gestation'] == SHARK_GESTATION
    True
    >>> fish['energy'] == SHARK_ENERGY
    True
    '''
    global NB_SHARKS
    NB_SHARKS= NB_SHARKS +1 
    return { 'char' : SHARK, 'gestation' : SHARK_GESTATION, 'energy' : SHARK_ENERGY }

def create_tuna():
    '''
    create a tuna data, it is a dictionnary with 2 fields :
    'char'acter for display and 'gestation' period
    @return the created tuna
    >>> fish = create_tuna()
    >>> fish['char'] == TUNA
    True
    >>> fish['gestation'] == TUNA_GESTATION
    True
    '''
    global NB_TUNAS
    NB_TUNAS = NB_TUNAS + 1
    return { 'char' : TUNA, 'gestation' : TUNA_GESTATION }


def create_sea(width, height):
    '''
    create a sea of size (width x height)
    a sea is a list of width lists of height squares
    squares are None if empty or a dictionnary representing a shark or a tuna as created by cretae_shark() or create_tuna()
    at creation, all squares are empty
    @return the created sea
    >>> create_sea(2,3)
    [[None, None, None], [None, None, None]]
    '''
    global SEA_WIDTH
    global SEA_HEIGHT
    SEA_WIDTH = width
    SEA_HEIGHT = height
    return [ [ None for _ in range(height)] for _ in range(width) ]


def init_sea(sea, proba_tuna, proba_shark):
    '''
    create sharks and tunas in the sea, for each square the proba that a tuna exist
    is proba_tuna and it is proba_shark for a shark
    sea state is modified by this function
    '''
    for x in range(SEA_WIDTH):
        for y in range(SEA_HEIGHT):
            alea = random.randrange(100)
            if alea < proba_tuna:
                sea[x][y] = create_tuna()
            elif alea < proba_tuna + proba_shark:
                sea[x][y] = create_shark()

def to_string(sea):
    '''
    build a string representation of the sea
    @return the built string 
    '''
    result = ''
    #result = result + '_'*(2*SEA_WIDTH+1)+'\n'
    result = result + '. '*(SEA_WIDTH+1)+'\n'  # a first line of '. '
    for x in range(SEA_WIDTH):
        result = result + '.'
        for y in range(SEA_HEIGHT):
            result = result + char_from_square(sea[x][y]) + '.'
        result = result + '\n'
    result = result + '_'*(2*SEA_WIDTH+1) + '\n'
    return result
        
def char_from_square(square):
    '''
    @return a character corresponding to the sea square parameter
    '''
    if not is_empty(square) :
        return square['char']
    return EMPTY


def is_empty(square):
    '''
    @return True iff square is empty
    '''
    return square == None
def is_tuna(square):
    '''
    @return True iff square contains a tuna
    '''
    return square != None and square['char'] == TUNA
def is_shark(square):
    '''
    @return True iff square contains a shark
    '''
    return square != None and square['char'] == SHARK



def evolve_one_square(sea):
    '''
    randomly pick a square of sea and if it contains a fish, behaviour rules are applied
    '''
    # randomly pick square coordinates
    (x,y) = (random.randrange(SEA_WIDTH), random.randrange(SEA_HEIGHT))    
    square = sea[x][y]
    # fish or not ?
    if is_empty(square) :
        return                   # nothing to do
    else:
        # there is a fish
        if is_tuna(square):
            # try to move tuna fish
            new_coord = tuna_move_to_empty_neighbour(sea, (x,y))
            # manage gestation time and possible birth
            manage_gestation(sea, (x,y), new_coord)        
        else: # is_shark(square)
            (x_dest, y_dest) = shark_move_to_neighbour(sea, (x,y) )
            if sea[x_dest][y_dest]['energy'] == 0:
                # shark has not enough energy, it dies
                sea[x_dest][y_dest] = None
                global NB_SHARKS
                NB_SHARKS = NB_SHARKS - 1
            else:
                # manage gestation time and possible birth        
                manage_gestation(sea, (x,y), (x_dest, y_dest))        


        
def manage_gestation(sea, old_coord, new_coord):
    '''
    manage gestation for fish that has tried to move from old_coord to new_coord (no move if coord are equals)
    gestation time is decreased, if it reaches 0 a new baby fish can bred if parent fish had moved, baby fish then spawns at old_coord
    if parent has not moved, baby fish can not born since there is no empty square
    '''
    (x_dest, y_dest) = new_coord
    fish = sea[x_dest][y_dest]
    # decrease gestation
    fish['gestation'] = fish['gestation'] - 1
    if fish['gestation'] == 0:          # gestation time finished, new fish can be bred ?
        # reset gestation time
        if is_tuna(fish):
            fish['gestation'] = TUNA_GESTATION
        else:
            fish['gestation'] = SHARK_GESTATION
        (x,y) = old_coord
        if (x,y) != (x_dest,y_dest):
            # fish has moved, new baby fish can be bred at previously occupied square since it is empty
            if is_tuna(fish):
                sea[x][y] = create_tuna()
            else:
                sea[x][y] = create_shark()
        #else: no empty square for new fish, it cannot be bred
        
        
def tuna_move_to_empty_neighbour(sea, coord) :
    '''
    try to move tuna at coord to a neighbour empty square if any, nothing happen if none
    @return the coord of the square occupied by the fish atthe end of this function, ie. new destination if it has moved or coord if not
    '''
    empty_neighbours = [ (x,y) for (x,y) in neighbour_coords(coord) if is_empty(sea[x][y]) ]    
    if empty_neighbours != []:
        (x,y) = coord
        tuna = sea[x][y]
        # move to a randomly chosen empty neighbour
        (x_dest, y_dest) = random.choice(empty_neighbours)
        # tuna moves to beighbour
        sea[x_dest][y_dest] = tuna
        # previous square becomes empty
        sea[x][y] = None
        return (x_dest, y_dest)
    else: #do not move coord remains the same
        return coord

def shark_move_to_neighbour(sea, coord) :
    '''
    try to move shark at coord to a neighbour square that must be empty or occupied by a tuna
    if it is a tun square, shark eats it and restores its energy
    @return the coord of the square occupied by the fish atthe end of this function, ie. new destination if it has moved or coord if not
    '''
    neighbours = [ (x_n,y_n) for (x_n,y_n) in neighbour_coords(coord) if not is_shark(sea[x_n][y_n]) ]
    (x,y) = coord
    shark = sea[x][y]
    # shark loses one energy
    shark['energy'] = shark['energy'] - 1
    if neighbours != []:
        # search first for tuna in neighborhood
        neighbours_with_tuna = [ (x_n,y_n) for (x_n,y_n) in neighbours if is_tuna(sea[x_n][y_n]) ]
        if not neighbours_with_tuna == [] : # tuna found
            # randomly chose one tuna
            (x_dest, y_dest) = random.choice(neighbours_with_tuna)            
            # there is a tuna at dest => shark eats, energy is restored
            shark['energy'] = SHARK_ENERGY
            global NB_TUNAS
            NB_TUNAS = NB_TUNAS - 1
        else:
            # no tuna in neighbourhood randomly pick an empty neighbour
            (x_dest, y_dest) = random.choice(neighbours)
        # shark moves to neighbour
        sea[x_dest][y_dest] = shark
        # previous square becomes empty
        sea[x][y] = None
        return (x_dest, y_dest)
    else: #do not move, coord remains the same
        return coord




def neighbour_coords(coord):
    '''
    '''
    def toric_coord(x, y):
        '''
        '''
        x = (SEA_WIDTH + x) % SEA_WIDTH
        y = (SEA_HEIGHT + y) % SEA_HEIGHT
        return (x,y)
    (x,y) = coord    
    return [ toric_coord(x-1,y), toric_coord(x+1,y), toric_coord(x,y-1), toric_coord(x, y+1) ]





    
if __name__ == '__main__':
    import doctest
    doctest.testmod()