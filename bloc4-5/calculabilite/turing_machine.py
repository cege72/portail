#!/usr/bin/python3
# -*- coding: utf-8 -*-

__author__ = 'FIL - Faculté des Sciences et Technologies -  Univ. Lille <http://portail.fil.univ-lille1.fr>_'
__date_creation__ = 'Oct  3  2020'
__doc__ = """
:mod:`turing_machine` module
:author: {:s} 
:creation date: {:s}
:last revision:

""".format(__author__, __date_creation__)

import graphviz

class TapeError(Exception):
    def __init__(self, msg):
        self._message = msg
        
class Tape():
    '''
    class for tapes of Turing machines.
    A tape is potentially infinite in both directions and is divised in cells, each cell 
    containing a symbol. Symbols on a tape may be any character.
    A special character is considered as a blank symbol. When a cell contains this blank symbol, 
    
    By default a tape is empty and contains only blank symbols.
    >>> empty_tape = Tape()
    >>> str(empty_tape)
    '...| | | | | | | | | | | | | | | | | | | | > < | | | | | | | | | | | | | | | | | | | | | | | | | | | | |...'
    >>> another_empty_tape = Tape(blank_symbol='0')
    >>> str(another_empty_tape)
    '...|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0>0<0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|...'
    
    A tape may be initialized with a content which may be any string.
    >>> non_empty_tape = Tape(content='eric', blank_symbol='_')
    >>> str(non_empty_tape)
    '...|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_>e<r|i|c|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|...'
    
    Cells of a tape are indexed by integers. When a tape is construced with a non empty content, the consecutive characters 
    of the string are placed at indices 0, 1, 2, ...
    >>> ''.join(non_empty_tape[i] for i in range(-2, 6))
    '__eric__'
    
    Any cell of a tape can be modified.
    >>> non_empty_tape[4] = 'W'
    >>> ''.join(non_empty_tape[i] for i in range(5))
    'ericW'
    '''
    def __init__(self, content='', blank_symbol=' '):
        if not isinstance(content, str):
            raise TapeError('content of a tape must be a string')
        if not isinstance(blank_symbol, str) or len(blank_symbol) != 1:
            raise TapeError('blank symbol must be a character')
        self._content = dict(enumerate(content)) if content!='' else {0 : blank_symbol}
        self._blank_symbol = blank_symbol
        
    def str(self, offset=-20, pos=0):
        if offset <= pos < offset+50:
            return ('...|' + '|'.join(self[i] for i in range(offset, pos)) +
                    '>{:s}<'.format(self[pos]) +
                    '|'.join(self[i] for i in range(pos+1, offset+50)) + '|...')
        else:
            return '...|' + '|'.join(self[i] for i in range(offset, offset+50)) + '|...'
    
    def __str__(self):
        return self.str()

    def __getitem__(self, index):
        try:
            return self._content[index]
        except KeyError:
            return self._blank_symbol
        
    def __setitem__(self, index, symb):
        if not isinstance(symb, str) or len(symb) != 1:
            raise TapeError('Cells of a tape can only contain a character')
        self._content[index] = symb
    
    def extremal_pos(self):
        return (min(k for k in self._content), max(k for k in self._content))
    
def _valid_transitions_table(obj):
    '''
    >>> _valid_transitions_table(1)
    False
    >>> _valid_transitions_table({})
    True
    >>> _valid_transitions_table({1:2})
    False
    >>> _valid_transitions_table({('A', '0') : ('1', -1, 'STOP')})
    True
    '''
    def _valid_key(key):
        return isinstance(key[0], str) and isinstance(key[1], str) and len(key[1]) == 1
    
    def _valid_value(val):
        return (isinstance(val, tuple) and
                len(val) == 3 and
                isinstance(val[0], str) and
                len(val[0]) == 1 and
                val[1] in {-1, 0, 1} and
                isinstance(val[2], str))
    
    return (isinstance(obj, dict) and
            all(isinstance(key, tuple) and len(key) == 2 for key in obj) and
            all(_valid_key(key) for key in obj) and
            all(_valid_value(obj[key]) for key in obj))

class TuringMachineError(Exception):
    def __init__(self, msg):
        self._message = msg
        
    
class TuringMachine():
    '''
    A Turing machine is a finite state machine equipped with an infinite tape divided into cells and a head that can read or write 
    symbols in cells of the tape. A transition table define the rules that the machine must follow. Rules are of the form
             (st1, symb1) -> (symb2, mvt, st2)
    meaning that if the machine is in state st1 and the head reads symbol symb1 on the tape, then 
      * the head has to write the symbol symb2 on the tape, 
      * machine moves the head to the left, or to the right or stay on the current cell of the tape according to the value of mvt, 
      * and then the machine must go to the state st2.
    '''
    
    def __init__(self, transitions, initial_state='A', blank_symbol=' '):
        if not _valid_transitions_table(transitions):
            raise TuringMachineError('invalid transitions table')
        if not isinstance(initial_state, str):
            raise TuringMachineError('initial state must be a string')
        self._transitions = transitions
        self._initial = initial_state
        self._state = initial_state
        self._tape = Tape(blank_symbol=blank_symbol)
        self._blank_symbol = blank_symbol
        self._nb_steps = 0
        self._pos = 0

    _FORMAT = '{:3d} : {:10s} {:3d} : {:s}'
    def run(self, tape=None, pos=0, initial_state='A', verbose=True, max_step=1<<50):
        self._tape = Tape(blank_symbol=self._blank_symbol) if tape is None else Tape(tape, blank_symbol=self._blank_symbol)
        self._pos = pos
        self._state = initial_state
        self._nb_steps = 0
        finished = self._nb_steps == max_step
        if verbose:
            print(TuringMachine._FORMAT.format(self._nb_steps, self._state, 
                                               self._pos, self._tape.str(pos=self._pos)))
        while not finished:
            symb = self._tape[self._pos]
            try:
                new_symb, mvt, new_state = self._transitions[self._state, symb]
            except KeyError:
                finished = True
            else:
                self._tape[self._pos] = new_symb
                self._state = new_state
                self._pos = self._pos + mvt
                self._nb_steps += 1
                finished = self._nb_steps == max_step
                if verbose:
                    print(TuringMachine._FORMAT.format(self._nb_steps, self._state, 
                                                       self._pos, self._tape.str(pos=self._pos)))
                    
    def get_state(self):
        return self._state
    
    def get_nb_steps(self):
        return self._nb_steps
    
    def get_tape(self):
        return self._tape
    
    def get_pos(self):
        return self._pos
    
    def to_latex(self):
        symb_to_latex = lambda symb: '\\mathtt{{{:s}}}'.format(symb) if symb!=self._blank_symbol else '\\diamond'
        tape = self.get_tape()
        min_pos, max_pos = tape.extremal_pos()
        current_pos = self.get_pos()
        current_state = self.get_state()
        return '\\,^\\omega\\diamond {:s}({:s}\\,{:s}){:s}\\diamond^\\omega'.format(
            ''.join(symb_to_latex(tape[k]) for k in range(min_pos, current_pos)),
            current_state,
            symb_to_latex(tape[current_pos]),
            ''.join(symb_to_latex(tape[k]) for k in range(current_pos+1, max_pos+1))
        )
        
    def to_dot(self, background_color='#FFFFFF'):
        nodes = set()
        edges = set()
        for state, symb in self._transitions:
            new_symb, mvt, new_state = self._transitions[(state, symb)]
            direction = 'D' if mvt == 1 else 'G' if mvt == -1 else '-'
            nodes.add(state)
            nodes.add(new_state)
            edges.add((state, new_state, "{:s},{:s},{:s}".format(symb if symb!=' ' else 'ESP', 
                                                                 new_symb, 
                                                                 direction)))
        edges = [ "{} -> {}[label=\"{}\"];".format(state1, state2, label) 
                 for state1, state2, label in edges ]
        descr = """
digraph {{
\tbgcolor="{:s}";
\trankdir=LR;

\t /* les noeuds */      
\tnode [shape=point, color=white, fontcolor=white];
\tstart;
\tnode [shape=circle, color=black, fontcolor=black];

\t /* les arcs */
\t{:s}
\tstart -> {:s} [label="start"];
\t{:s}
}}""".format(background_color,
            ";".join(list(nodes)), 
             self._initial, 
             "\n\t".join(list(edges)))
        return descr
    
    def show_transition_graph(self, background_color='#FFFFFF', filename='mt'):
        graphviz.Source(self.to_dot(background_color), format='png').view(filename=filename)
        
if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)


