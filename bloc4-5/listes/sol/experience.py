from list import List
from extendedlistsol import Extendedlist

import timeit
import pylab
import numpy as np

def somme_des_elements_a (l):
    s = 0
    for i in range(len(l)):
        s += l[i]
    return s

def somme_des_elements_b (l):
    s = 0
    for e in l:
        s += e
    return s

def experience (maxlen,step):

    x = []
    acces_i_el = []
    iterateur_el = []
    acces_i_lp = []
    iterateur_lp = []
    
    for n in range(1,maxlen+2,step):
        print(n)
        
        l = Extendedlist()
        for i in range(n):
            l = Extendedlist(i,l)

        lp = l.toPythonList()

        # partie comparaison iteration vs. accès au i-eme
        d1 = timeit.timeit(lambda: somme_des_elements_a(l),number = 1)

        d2 = timeit.timeit(lambda: somme_des_elements_b(l), number = 1)

        d3 = timeit.timeit(lambda: somme_des_elements_a(lp), number = 1)

        d4 = timeit.timeit(lambda: somme_des_elements_b(lp), number = 1)

        x.append(n)
        acces_i_el.append(d1)
        iterateur_el.append(d2)
        acces_i_lp.append(d3)
        iterateur_lp.append(d4)

    for i in range(len(x)):
        print("{} {} {} {} {} {} {}".format(x[i],acces_i_el[i],iterateur_el[i],
                                            acces_i_lp[i],acces_i_el[i],
                                            acces_i_el[i]/iterateur_el[i],
                                            acces_i_lp[i]/iterateur_lp[i]))
    pylab.semilogy(x, acces_i_el)
    pylab.semilogy(x, acces_i_lp)
    pylab.semilogy(x, iterateur_el)
    pylab.semilogy(x, iterateur_lp)
    pylab.legend(["[i] Extendedlist", "[i] Python", "it Extendedlist", "it Python"])
    pylab.show()

    r = np.linspace(1,maxlen,step)

    pylab.plot(x, acces_i_el)
    pylab.plot(x, iterateur_el)
    y1 = 1e-7*r
    y2 = 0.5e-6*r**2
    pylab.plot(r,y1)
    pylab.plot(r,y2)
    pylab.legend(["[i] Extendedlist", "it Extendedlist", "f(x)=1e-7*x", "f(x)=0.5e-6*x^2"])
    pylab.show()

    pylab.plot(x, acces_i_lp)
    pylab.plot(x, iterateur_lp)
    y1 = 1e-7*r
    pylab.plot(r,y1)
    pylab.legend(["[i] Python", "it Python", "f(x)=1e-7*x"])
    pylab.show()
    
if __name__ == "__main__":
    experience(3000,500)
    
