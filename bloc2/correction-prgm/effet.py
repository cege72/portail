## Illustration du fait que deux appels successifs à une fonction avec les
## mêmes arguments peuvent donner des résultats différents. La raison ici est
## que la fonction modifie la valeur d'une variable globale.
##
## Lorsque l'on programme il faut être attentif aux effets de bord
## (modification de la mémoire, affichage, etc...). Ceux-ci modifient
## l'environnement pour tous les composants d'un programme :
##
## 1. Il faut veiller à ce que cela se fasse dans le bon ordre.
##
## 2. Il faut bien documenter les fonctions qui ont des effets de bord.
##
## 3. Il faut limiter autant que possible que les fonctions aient des effets de
## bord afin de faciliter le raisonnement sur le code.

x=0
def effect(n):
    global x
    x=n+x
    return x

effect(1)
effect(1)
